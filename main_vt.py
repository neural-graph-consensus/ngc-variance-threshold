import sys
import importlib.util
import numpy as np
import yaml
from typing import Callable
from pathlib import Path
from argparse import ArgumentParser

from ngclib.models import getModel
from nwutils.path import change_directory
from vt import compute_variance_thresholds

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--graph_nodes_path", required=True, help="Module where we load the " \
        "getNodes(graph_cfg) function")
    parser.add_argument("--ngc_dir_path", required=True)
    parser.add_argument("--graph_cfg_path", required=True)
    parser.add_argument("--dataset_path", required=True)
    parser.add_argument("--output_dir", default=".")
    parser.add_argument("--debug", type=int, default=0)
    parser.add_argument("--iteration", type=int, default=1)
    parser.add_argument("--n_bins", type=int, default=20)
    args = parser.parse_args()

    args.output_dir = Path(args.output_dir).absolute()
    args.dataset_path = Path(args.dataset_path).absolute()
    args.debug = bool(args.debug)
    args.graph_nodes_path = Path(args.graph_nodes_path).absolute()
    args.ngc_dir_path = Path(args.ngc_dir_path).absolute()
    return args

def load_get_nodes_module(graph_nodes_path: Path) -> Callable:
    sys.path.append(str(graph_nodes_path.parent))
    spec = importlib.util.spec_from_file_location("module.name", graph_nodes_path)
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)
    names = [x for x in foo.__dict__]
    globals().update({k: getattr(foo, k) for k in names})
    return foo.getNodes

def plot_analysis(node_analysis: np.ndarray, out_file: Path, thresholds: dict = None):
    df = pd.DataFrame(node_analysis, columns=["Variance", "Ens - SL", "Ens"])
    df["SL"] = df["Ens"] - df["Ens - SL"]
    logger.debug(f"Stored plot of analysis at '{out_file}'")
    fig = px.line(df, x="Variance", y=["Ens - SL", "Ens", "SL"])

    if thresholds is not None:
        dfv = df["Variance"]
        dfe = df["Ens - SL"]
        ixs = [(dfv - thresholds[i]).abs().argmin() for i in range(len(thresholds))]
        x = [dfv[ix] for ix in ixs]
        y = [dfe[ix] for ix in ixs]
        fig2 = px.scatter(x=x, y=y)
        fig = go.Figure(data=fig.data + fig2.data)
        title = "; ".join([f"{dfv[ix]:.3f} ({ix}/{len(dfe)})" for ix in ixs])
        fig.update_layout(title=title)
        # fig.update_layout(title=f"Min: {dfv[ix_min]:.3f}. Threshold: {dfv[ix_best]:.3f} ({ix_best}/{len(dfe)})")

    fig.write_image(out_file)

def main():
    args = get_args()

    graph_cfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    assert graph_cfg["voteFunction"] == "threshold_based_selection", graph_cfg["voteFunction"]
    graph_cfg["voteFunction"] = "simpleMedian"

    get_nodes_fn = load_get_nodes_module(args.graph_nodes_path)
    model = getModel(get_nodes_fn(graph_cfg), graph_cfg)

    print(model.summary())
    weights_path = args.ngc_dir_path / f"iter{args.iteration}/models"

    change_directory(args.output_dir)
    thresholds = compute_variance_thresholds(model, weights_path, args.dataset_path, args.n_bins, args.debug)
    print(thresholds)


    print(f"Stored to '{args.output_dir}/var_thresholds.npy'.")
    np.save(f"{args.output_dir}/var_thresholds.npy", thresholds)
    return thresholds

if __name__ == "__main__":
    main()