"""Analysise a variance result (var_node.npy => var_analysis_node.npy) """
import numpy as np
from pathlib import Path
from argparse import ArgumentParser
from ngclib.utils import getFakeNodeType
from nwgraph.node import Node, RegressionNode, ClassificationNode
from typing import List, Dict

from vt import get_threshold_from_analysis, plot_analysis

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--in_dir", required=True)
    parser.add_argument("--nodes", required=True)
    parser.add_argument("--n_bins", default=100, type=int)
    args = parser.parse_args()
    args.nodes = args.nodes.split(",")
    return args

def get_node(node_name: str) -> Node:
    node = {
        "depthSfm": RegressionNode,
        "ngcSegprop": ClassificationNode,
        "normalsSfm": RegressionNode
    }[node_name]
    node_args = {
        RegressionNode: {"name": node_name},
        ClassificationNode: {"name": node_name, "classes": 8}
    }[node]
    node = getFakeNodeType(node)(**node_args)
    return node

def get_analysis_from_files(in_dir: Path, nodes: List[Node], num_bins: int) -> Dict[str, np.ndarray]:
    in_files = {node: in_dir / Path(f"var_analysis_{node.name}_{num_bins}.npy") for node in nodes}
    print(f"In files: {in_files}")
    analysis = {node: np.load(path) for node, path in in_files.items()}
    return analysis

def get_min_before_threshold(node_name, analysis, threshold = None):
    if threshold is None:
        threshold = get_threshold_from_analysis(node_name, analysis)
    last_ix = np.where(analysis[:, 0] <= threshold)[0][-1]
    analysis_min = analysis[0 : last_ix]
    ix_analysis_min = analysis_min[:, 0][analysis_min[:, 1].argmin()]
    return ix_analysis_min

def main():
    args = get_args()
    nodes = [get_node(node_name) for node_name in args.nodes]

    in_dir = Path(args.in_dir).absolute()
    analysis = get_analysis_from_files(in_dir, nodes, args.n_bins)

    thresholds = {node: get_threshold_from_analysis(node, analysis[node]) for node in analysis}
    [plot_analysis(analysis[node], f"var_analysis_{node}_{args.n_bins}.png", [thresholds[node]]) for node in analysis]

    # analysis_path = Path(in_file.parent / f"var_analysis_{node.name}_{args.n_bins}.npy")
    # analysis = get_analysis(analysis_path, node, inputs, args.n_bins)

    # threshold = get_threshold_from_analysis(node.name, analysis)
    # print(f"Best threshold: {threshold:.3f}")

    # min_before_threshold = get_min_before_threshold(node.name, analysis, threshold)
    # print(f"Min before: {min_before_threshold:.3f}")

    # plot_analysis(analysis, f"var_analysis_{node}.png", thresholds=[min_before_threshold, threshold])

if __name__ == "__main__":
    main()