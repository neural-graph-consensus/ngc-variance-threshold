from typing import List, Dict
from pathlib import Path
import torch as tr
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from nwutils.torch import trGetData, trToDevice
from nwmodule.loss import softmax_nll, l2
from nwgraph import Node, RegressionNode, ClassificationNode

from ngclib.models import NGC
from ngclib.logger import logger, drange
from ngclib.readers import NGCNpzReader
from ngclib.voting_algorithms import ThresholdBasedSelection
from ngclib.trainer import EdgeTrainer

## Step 1

def pass_once_validation_set(model, required_nodes: List, validation_dir: Path, debug: bool):
    """Pass the validation set, get the variances per pixel, as well as the loss for single link and ensemble"""
    prev = tr.are_deterministic_algorithms_enabled()
    tr.use_deterministic_algorithms(False)
    input_nodes = [model.name_to_node[x] for x in model.cfg["inputNodes"]]
    validation_reader = NGCNpzReader(path=validation_dir, nodes=model.nodes, gtNodes=input_nodes)
    debug_subset = 10 if debug else None
    loader = validation_reader.toDataLoader(randomize=False, debug=debug, num_workers=0, debug_subset=debug_subset)
    iterator = iter(loader)
    assert len(loader) > 0

    f_criterion = {node: get_criterion(node) for node in required_nodes}
    tbs = ThresholdBasedSelection(model)

    result_variance = {k: np.empty((0, 3)) for k in required_nodes}
    for _ in drange(len(loader), desc="Threshold Based Selection"):
        data = trToDevice(trGetData(next(iterator)), model.getDevice())
        x, gt = data["data"], data["labels"]
        with tr.no_grad():
            y = model.forward(x)
        for node in required_nodes:
            node_gt = gt[node.name]
            messages = y[node]
            assert len(messages) == 1, messages
            message = list(messages)[0]
            assert message.path[-1] == "Vote"
            node_input = message.input.detach()
            node_output = message.output.detach()
            variance = tbs.variance_from_results(node, node_input).detach().cpu().numpy().flatten()

            assert len(message.input) > 1, f"Node '{node}' has just one output message"
            ix = tuple(map(lambda x: x.startswith("Single Link (rgb"), message.path[0].split("|")))
            assert sum(ix) == 1
            ix = ix.index(True)
            sl_loss = f_criterion[node](node_input[ix], node_gt).cpu().numpy().flatten()
            y_loss = f_criterion[node](node_output, node_gt).cpu().numpy().flatten()
            result_variance[node.name] = np.append(result_variance[node.name],
                np.stack([variance, y_loss, sl_loss], axis=-1), axis=0)
    tr.use_deterministic_algorithms(prev)

    for node in required_nodes:
        inputs = np.array(result_variance[node.name]), node
        assert len(inputs) > 0
    return result_variance

def compute_variances_threshold(model: NGC, weights_path: Path, validation_dir: Path, debug: bool) -> Dict:
    """Step 1: Computes the VTs of all output nodes by passing the validation set once."""
    input_nodes = [model.name_to_node[x] for x in model.cfg["inputNodes"]]
    output_nodes = set(model.nodes).difference(input_nodes)
    required_nodes = []
    result_variance = {}
    for node in output_nodes:
        npz_file = Path(f"var_{node}.npy")
        if npz_file.exists():
            result_variance[node.name] = np.load(npz_file)
        else:
            required_nodes.append(node)
    logger.debug(f"Required nodes for variance threshold computation: {required_nodes}")

    if len(required_nodes) > 0:
        # Load weights only if required to save some time.
        file_name = {edge: ("model_best_F1 Score.pkl" if isinstance(edge.outputNode, ClassificationNode)
            else "model_best_Loss.pkl") for edge in model.edges}
        model.loadAllEdges(weights_path, EdgeTrainer.getEdgeDirName, file_name)
        model.eval()

        new_nodes = pass_once_validation_set(model, required_nodes, validation_dir, debug)
        result_variance = {**result_variance, **new_nodes}
    for node in required_nodes:
        npz_file = Path(f"var_{node}.npy")
        np.save(npz_file, result_variance[node.name])
    return result_variance

## Step 2

def get_criterion(node):
    if isinstance(node, RegressionNode):
        return l2
    elif isinstance(node, ClassificationNode):
        # Reverse accuracy
        return lambda y, gt: y.argmax(-1) != gt.argmax(-1)
    assert False, node

def get_reduce_function(node):
    if isinstance(node, RegressionNode):
        return np.median
    elif isinstance(node, ClassificationNode):
        return np.mean
    assert False, node

def compute_variance_analysis(node: Node, vars, y_ens, y_sl, n_bins: int):
    """For each bin (i.e. 20 bins default) from 0 to maximum variance, get the median error of that particular bin
    for both the ensemble and the single link. Return the starting variance as well as the errors"""
    ls = np.linspace(0, vars.max(), n_bins + 1)
    bins_l = ls[0:-1]
    bins_r = ls[1:]

    res = []
    reduce_function = get_reduce_function(node)
    logger.debug(f"Reduce function for node '{node}' each bin: '{reduce_function}'")
    for i in range(n_bins):
        Where = np.where((vars >= bins_l[i]) & (vars < bins_r[i]))
        y_ens_bin = y_ens[Where]
        y_sl_bin = y_sl[Where]
        if isinstance(node, RegressionNode):
            sl_res = reduce_function(y_sl_bin)
            ens_res = reduce_function(y_ens_bin)
        elif isinstance(node, ClassificationNode):
            sl_res = reduce_function(y_sl_bin)
            ens_res = reduce_function(y_ens_bin)

        logger.debug2(f"[{bins_l[i]}, {bins_r[i]}) Cnt: {Where[0].sum()}, Ens: {ens_res}, SL: {sl_res}")
        res.append([bins_l[i], ens_res - sl_res, ens_res])
    res = np.array(res)
    res[np.isnan(res)] = 0
    return res

def compute_analysis_from_variance(node: Node, variances: np.ndarray, n_bins: int, file_name: Path) -> np.ndarray:
    if file_name.exists():
        logger.debug(f"Node '{node.name}' analysis exists. Loading '{file_name}'.")
        return np.load(file_name)

    logger.debug(f"Computing '{node.name}' analysis and saving at '{file_name}'")
    vars, y_ens, y_sl = variances[:, 0], variances[:, 1], variances[:, 2]
    analysis = compute_variance_analysis(node, vars, y_ens, y_sl, n_bins)
    np.save(file_name, analysis)
    return analysis

def compute_analysis_from_variances(nodes: List[Node], result_variance: Dict[str, np.ndarray], n_bins: int=20):
    """Step 2. Get the analysis (mean/median) of each node over the requied bins, for ensemble and SL"""
    logger.debug("Performing variance analysis for all output nodes")
    name_to_node = {node.name: node for node in nodes}
    analysis = {}
    output_nodes_names = result_variance.keys()
    for node_name in output_nodes_names:
        node = name_to_node[node_name]
        variances = result_variance[node_name]
        npz_file = Path(f"var_analysis_{node_name}_{n_bins}.npy")
        analysis[node_name] = compute_analysis_from_variance(node, variances, n_bins, npz_file)
    return analysis

## Step 3

def get_threshold_from_analysis(node_name: str, analysis: np.ndarray):
    """Step 3. Get the best threshold from the analysis (bins reductions from all variances)"""
    variances, ensemble_minus_sl_loss = analysis[:, 0:2].T
    assert len(variances) == len(ensemble_minus_sl_loss), f"{len(variances)} vs {len(ensemble_minus_sl_loss)}"
    assert len(variances) >= 10
    """
    - From all the variances' losses, i.e.
    variances: [0.   , 0.015, 0.029, 0.044, 0.058, 0.073, 0.088, 0.102, 0.117,
    0.132, 0.146, 0.161, 0.175, 0.19 , 0.205, 0.219, 0.234, 0.248,
    0.263, 0.278]
    losses diff: [-0.013, -0.278, -0.325, -0.277, -0.266, -0.253, -0.23 , -0.19 ,
    -0.163, -0.104, -0.056, -0.294, -0.298, -0.129,  0.099,  0.254,
        0.125, -1.63 , -2.527, -3.049]

    - Find these that are above 0 (sl is better).
    where_relevant: [14, 15, 16]

    - If none relevant, return last variance bin (variances[-1])

    - Get the gradient of these bins, to check that two consecutive ones are happening:
    grad_relevant: [1, 1]
    where_grad_relevant: [0, 1]

    - If there are no consecutive ones, get the last get the last bin: variances[grad_relevant[-1]]

    - Otherwise, get the first bin where this is happening: variances[where_relevant[where_grad_relevant[0]]]

    """
    where_relevant = np.where(ensemble_minus_sl_loss > 0)[0]
    if len(where_relevant) in (0, 1):
        logger.debug(f"Node '{node_name}'. All variances are below 0, ensemble is always better.")
        return variances[-1]
    grad_relevant = where_relevant[1:] - where_relevant[0:-1]
    where_grad_relevant = np.where(grad_relevant == 1)[0]
    if len(where_grad_relevant) == 0:
        ix = where_relevant[-1]
        logger.debug(f"Node '{node_name}'. No consistent threshold, getting the last variance "
                    f"(ix: {ix}/{len(variances)}) where ensemble didn't perform: {variances[ix]}")
        return variances[ix]
    ix = where_relevant[where_grad_relevant[0]]
    logger.debug(f"Node '{node_name}'. At least one relevant theshold, (ix: {ix}/{len(variances)}) "
                f"where ensemble didn't perform: {variances[ix]}")
    return variances[ix]

## Step 4. Plot the results

def plot_analysis(node_analysis: np.ndarray, out_file: Path, thresholds: dict = None):
    df = pd.DataFrame(node_analysis, columns=["Variance", "Ens - SL", "Ens"])
    df["SL"] = df["Ens"] - df["Ens - SL"]
    logger.debug(f"Stored plot of analysis at '{out_file}'")
    fig = px.line(df, x="Variance", y=["Ens - SL", "Ens", "SL"])

    if thresholds is not None:
        dfv = df["Variance"]
        dfe = df["Ens - SL"]
        ixs = [(dfv - thresholds[i]).abs().argmin() for i in range(len(thresholds))]
        x = [dfv[ix] for ix in ixs]
        y = [dfe[ix] for ix in ixs]
        fig2 = px.scatter(x=x, y=y)
        fig = go.Figure(data=fig.data + fig2.data)
        title = "; ".join([f"{dfv[ix]:.3f} ({ix}/{len(dfe)})" for ix in ixs])
        fig.update_layout(title=title)
    fig.write_image(out_file)

def compute_variance_thresholds(model: NGC, weights_path: Path, validation_dir: Path,
                                n_bins: int=20, debug: bool=False):
    """Computes the VTs of all output nodes"""
    print(f"Working directory: '{Path.cwd()}'")
    # Compute the variances on the validation set, then extract relevant thresholds.
    result_variance = compute_variances_threshold(model, weights_path, validation_dir, debug)
    analysis = compute_analysis_from_variances(model.nodes, result_variance, n_bins)
    thresholds = {node: get_threshold_from_analysis(node, analysis[node]) for node in analysis}
    [plot_analysis(analysis[node], f"var_analysis_{node}_{n_bins}.png", [thresholds[node]]) for node in analysis]

    logger.info(f"Thresholds: {thresholds}")
    return thresholds
